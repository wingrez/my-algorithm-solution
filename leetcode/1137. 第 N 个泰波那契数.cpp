class Solution {
public:
    int tribonacci(int n) {
        if (n < 2) return n;
        if (n == 2) return 1;
        int p1 = 0, p2 = 1, p3 = 1;
        int res = 0;
        for (int i = 3; i <= n; i++) {
            res = p1 + p2 + p3;
            p1 = p2;
            p2 = p3;
            p3 = res;
        } 
        return res;
    }
};