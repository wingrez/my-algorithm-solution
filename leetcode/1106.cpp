class Solution {
public:
    char And(vector<bool> &exp) {
        for (auto b: exp) {
            if (b == false) return 'f';
        }
        return 't';
    }

    char Or(vector<bool> &exp) {
        for (auto b: exp) {
            if (b == true) return 't';
        }
        return 'f';
    }

    char Not(vector<bool> &exp) {
        if (exp.size() != 1) cout << "err 3" << endl;
        if (exp[0] == true) return 'f';
        return 't';
    }

    bool parseBoolExpr(string expression) {
        stack<char> stk;
        vector<bool> exp; 
        for (char ch: expression) {
            if (ch == ',') continue;
            if (ch != ')') {
                stk.push(ch);
                continue;
            }
            exp.clear();
            while (!stk.empty()) {
                char cur = stk.top();
                stk.pop();

                cout << cur << endl;

                if (cur == 't') exp.push_back(true);
                else if (cur == 'f') exp.push_back(false);
                else if (cur == '(') {
                    char res;
                    if (stk.empty()) {
                        res = exp[0];
                    } else {
                        cur = stk.top();
                        stk.pop();

                        cout << cur << endl;
                        
                        if (cur == '&') res = And(exp);
                        else if (cur == '|') res = Or(exp);
                        else if (cur == '!') res = Not(exp);
                        else cout << "err 1" << endl;
                    }
                    stk.push(res);
                    break;
                } else cout << "err 2" << endl;
            }
        }

        return stk.top() == 't';
    }

};