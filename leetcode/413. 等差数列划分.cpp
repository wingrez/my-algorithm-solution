class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& nums) {
        if (nums.size() == 1) {
            return 0;
        }

        vector<int> diff(nums.size() - 1);
        for (int i = 0; i < nums.size() - 1; i++) {
            diff[i] = nums[i + 1] - nums[i];
        }

        int ans = 0;
        int cnt = 0;
        int pre = diff[0];
        for (int i = 1; i < diff.size(); i++) {
            if (pre == diff[i]) {
                cnt++;
            } else {
                if (cnt > 0) {
                    ans += cnt * (cnt + 1) / 2;
                }
                pre = diff[i];
                cnt = 0;
            }
        }

        if (cnt > 0) {
            ans += cnt * (cnt + 1) / 2;
        }

        return ans;
    }
};