class Solution {
public:
    vector<vector<int>> allCellsDistOrder(int R, int C, int r0, int c0) {
        vector<vector<int>> arr(R * C, vector<int>(2, 0));
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                arr[i * C + j][0] = i;
                arr[i * C + j][1] = j;
            }
        }
        sort(arr.begin(), arr.end(), [r0, c0](vector<int> &a, vector<int> &b) -> bool {
            int dis1 = abs(a[0] - r0) + abs(a[1] - c0);
            int dis2 = abs(b[0] - r0) + abs(b[1] - c0);
            return dis1 < dis2;
        });
        return arr;
    }
};