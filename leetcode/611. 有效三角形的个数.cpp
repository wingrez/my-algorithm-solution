class Solution {
public:
    int triangleNumber(vector<int>& nums) {
        int ans = 0;
        sort(nums.begin(), nums.end());
        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] == 0) continue;
            for (int j = i + 1; j < nums.size(); j++) {
                auto p = lower_bound(nums.begin(), nums.end(), nums[i] + nums[j]);
                ans += (p - nums.begin() - j - 1);
            }
        }
        return ans;
    }
};