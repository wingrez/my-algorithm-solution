class Solution {
public:
    string maximumTime(string time) {
        if (time[0] == '?') {
            if (time[1] >= '4' && time[1] <= '9') {
                time[0] = '1';
            } else {
                time[0] = '2';
            }
        }
        
        if (time[1] == '?') {
            switch (time[0]) {
                case '0':
                	time[1] = '9';
                    break;
                case '1':
                	time[1] = '9';
                    break;
                case '2':
                	time[1] = '3';
                    break;
                default:
                	break;
            }
        }

        if (time[3] == '?') {
            time[3] = '5';
        }

        if (time[4] == '?') {
            time[4] = '9';
        }

        return time;
    }
};