bool cmp(pair<int, int> &a, pair<int, int> &b) {
    if (a.second == b.second) {
        return a.first < b.first;
    }
    return a.second < b.second;
}

class Solution {
public:
    vector<int> kWeakestRows(vector<vector<int>>& mat, int k) {
        vector<pair<int, int>> rank;
        for (int i = 0; i < mat.size(); i++) {
            int num = 0;
            for (int j = 0; j < mat[i].size(); j++) {
                mat[i][j] == 1? num++ : 0;
            }
            rank.push_back({i, num});
        }
        sort(rank.begin(), rank.end(), cmp);

        vector<int> res;
        for (int i = 0; i < k; i++) {
            res.push_back(rank[i].first);
        }

        return res;
    }
};