class Solution {
public:
    string customSortString(string order, string s) {
        vector<int> seq(26, 26);
        for (int i = 0; i < order.size(); i++) {
            seq[order[i] - 'a'] = i;
        }

        sort(s.begin(), s.end(), [&](const auto &lhs, const auto &rhs){
            return seq[lhs - 'a'] < seq[rhs - 'a'];
        });

        return s;
    }
};