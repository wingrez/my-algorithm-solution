/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int findSecondMinimumValue(TreeNode* root) {
        int minVal = root->val;
        queue<TreeNode*> qu;
        qu.push(root);
        int nextMinVal = minVal;
        while(!qu.empty()) {
            TreeNode *p = qu.front();
            int v = p->val;
            qu.pop();

            if (nextMinVal == minVal) {
                nextMinVal = v;
            } else if (v != minVal) {
                nextMinVal = min(nextMinVal, v);
            }
            
            if (p->left != NULL) {
                qu.push(p->left);
            }
            if (p->right != NULL) {
                qu.push(p->right);
            }
        }

        if (nextMinVal != minVal) {
            return nextMinVal;
        }

        return -1;
    }
};