#include <stdio.h>
#include <string.h>

#define MAXN 100

int main() {
    char src[MAXN];
    int i, j;
    while (~scanf("%s", src)) {
        char dest[MAXN] = {'0'};
        int index = 0;
        for (i = strlen(src) - 1; i > 1; i--) {
            int num = src[i] - '0';
            int temp;
            for (j = 0; j < index || num; j++) {
                temp = num * 10 + (j < index ? dest[j] - '0' : 0);
                dest[j] = temp / 8 + '0';
                num = temp % 8;
            }
            index = j;
        }
        dest[j] = '\0';
        printf("%s [8] = 0.%s [10]\n", src, dest);
    }
    return 0;
}