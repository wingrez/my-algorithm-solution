#include <stdio.h>

int main() {
    char ch;
    while (1) {
        int sum = 0;
        while (scanf("%c", &ch) && ch != '\n') {
            sum += ch - '0';
        }
        if (sum == 0) break;
        if (sum % 9 == 0) sum = 9;
        else sum = sum % 9;
        printf("%d\n", sum);
    }
    return 0;
}