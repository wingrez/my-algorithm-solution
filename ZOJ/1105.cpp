#include <stdio.h>
#include <math.h>

double distance(double x1, double y1, double x2, double y2) {
    return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

int main() {
    double x0, y0;
    double x1, y1, x2, y2;
    char street[80];
    while (scanf("%lf%lf\n", &x0, &y0) != EOF) {
        double dist = 0;
        gets(street);
        while (street[0] != 'j') {
            sscanf(street, "%lf%lf%lf%lf", &x1, &y1, &x2, &y2);
            dist += distance(x1, y1, x2, y2);
            gets(street);
        }
        dist = 60 * 2 * dist / 20000.0;
        int spend = floor(dist + 0.5);
        printf("%d:%02d\n", spend / 60, spend % 60);
    }
    return 0;
}